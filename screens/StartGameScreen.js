import  React, {useState, useEffect} from "react";
import {
  View,
  StyleSheet,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import Card from "../components/Card";
import  Colors from "../constants/colors.js";
import Input from "../components/input";
import NumberContainer from "../components/NumberContainer.js"
import { TitleText, BodyText } from "../components/Text";
import CustomButton from "../components/Button";
const StartGameScreen = props=>{
    const [enteredNumber, setEnteredNumber] = useState('');
    const [userConfirm, setUserConfirm] = useState(false);
    const [selectedNumber, setSelectedNumber] = useState();
    const [buttonWidth, setButtonWidth] = useState(Dimensions.get("window").width / 4);
    
    useEffect(()=>{
        const buttonWidthHandler = ()=>{
            setButtonWidth(Dimensions.get("window").width / 4)
        }
        Dimensions.addEventListener("change", buttonWidthHandler)
        return()=>{
            Dimensions.removeEventListener("change", buttonWidthHandler)
        }
    })

    const numberInputHandler = numberValue=>{

        const validStringValue = numberValue.replace(/[^0-9]/g, "")
        setEnteredNumber(validStringValue);

    }


    const resetInputHandler = ()=>{
        setEnteredNumber("")
        setUserConfirm(false);
    }
    const confirmInputHandler = ()=>{

        const valueAsInteger = parseInt(enteredNumber);
        if (isNaN(valueAsInteger) || valueAsInteger <= 0 || valueAsInteger > 99){
            Alert.alert("Invalid Number!", "A number has to be number between 1-99", [{text: "Okay", style: "destructive", onPress: resetInputHandler}])
            return;
        }
        setUserConfirm(true);
        setEnteredNumber("");
        setSelectedNumber(valueAsInteger);
        Keyboard.dismiss();
    }
    let confirmComponent;
    if(userConfirm){
        confirmComponent = (
            <Card style={styles.summaryContainer}>
                <BodyText>You Selected</BodyText>
                <NumberContainer>{selectedNumber}</NumberContainer>
                <CustomButton onPress={()=>{props.ontStartGame(selectedNumber)}}>START GAME</CustomButton>
            </Card>
        )
    }
    return(
        <ScrollView>
        <KeyboardAvoidingView behavior="position">
        <TouchableWithoutFeedback onPress={()=>{Keyboard.dismiss()}}>
            <View style={styles.screen}>
                <TitleText style={styles.title}>Start a new game</TitleText>
                <View>
                    <Card style={styles.inputContainer}>
                        <BodyText>Select a Number</BodyText>
                        <Input 
                            style={styles.textInput} 
                            placeholder="Number" 
                            autoCapitalize="sentences"
                            blurOnSubmit ={true}
                            keyboardType="number-pad" 
                            autoCorrect={false}
                            maxLength={2}    
                            onChangeText={numberInputHandler}
                            value={enteredNumber}
                        />
                    
                        <View style={styles.buttonsContainer}>
                            <View  style={{...styles.button, width: buttonWidth}}>
                                <Button title="Reset" color={Colors.accent} onPress={resetInputHandler}/>
                            </View>
                            <View  style={{...styles.button, width: buttonWidth}}>
                                <Button title="Confirm" color={Colors.primary} onPress={confirmInputHandler}/>
                            </View>
                        </View>
                    </Card>
                    
                </View>
                {confirmComponent}
            </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    screen:{
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    buttonsContainer:{
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around"
    },
    inputContainer:{
        width: "80%",
        maxWidth: "95%",
        minWidth: 300,
        alignItems: "center",
    }, 
    title:{
        fontSize: 20,
        marginVertical: 10
    },
    textInput:{
        width: 50,
        textAlign: "center"

    },
    button:{
        //width: 100,
        width: Dimensions.get("window").width / 4,
        paddingVertical: 8,
        fontSize: 14,
        textAlign: "center"
    },
    summaryContainer: {
        marginVertical: 12,
        alignItems: "center"
    }

})

export default StartGameScreen;