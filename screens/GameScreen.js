import React, {useState, useRef, useEffect} from "react";
import {View, Text, StyleSheet, Button, Alert, ScrollView, FlatList, Dimensions} from "react-native";
import NumberContainer from "../components/NumberContainer";
import Card from "../components/Card";
import { BodyText } from "../components/Text";
import CustomButton from "../components/Button";
import {Ionicons} from "@expo/vector-icons"
const generateRandomNumberBetween=(min, max, exclude)=>{
    min = Math.ceil(min);
    max = Math.floor(max);
    const rndNumber = Math.floor((Math.random() * (max - min)) + min)
    if(rndNumber === exclude){
        return generateRandomNumberBetween(min, max, exclude)
    }
    
    return rndNumber;

}
const GameScreen = props=>{
    const initialGuess = generateRandomNumberBetween(1, 100, props.userChoice)
    const [currentGuess, setCurrentGuess] = useState(initialGuess)
    const [pastGuesses, setPastGuesses] = useState([initialGuess]);
    const currentLow = useRef(1);
    const currentHigh = useRef(100)

    const {userChoice, onGameOver} = props;
    useEffect(()=>{
        if(currentGuess === props.userChoice){
            onGameOver(pastGuesses)
        }
    }, [currentGuess, userChoice, onGameOver]);

    const nextGuessHandler = direction=>{
        if(
            (direction === "LOWER" && currentGuess < props.userChoice)
            ||
            (direction === "GREATER" && currentGuess > props.userChoice)
        ){
            Alert.alert("Don\'t Lie", "Your provided wrong hint! Whyyyyy!", [{text: "Fine", style: "cancel"}]);
            return;
        }
        if (direction === "LOWER"){
            currentHigh.current = currentGuess
        }
        else{
            currentLow.current = currentGuess
        }
        const nextNumber = generateRandomNumberBetween(currentLow.current, currentHigh.current, currentGuess);
        //setAttemptsCount(attemptsCount + 1)
        setPastGuesses(curPastGuesses => [nextNumber ,...curPastGuesses])
        setCurrentGuess(nextNumber);
    }
    return(
        <View style={styles.screen}>
            <BodyText>Opponent's Guess</BodyText>
            <NumberContainer>{currentGuess}</NumberContainer>
            <Card style={styles.buttonsContainer}>
                <CustomButton onPress={nextGuessHandler.bind(this, "LOWER")}><Ionicons name="md-remove" size={24} color="white" /></CustomButton>
                <CustomButton onPress={nextGuessHandler.bind(this, "GREATER")}><Ionicons name="md-add" size={24} color="white" /></CustomButton>
            </Card>
            <View style={styles.listContainer}>
                {/* <ScrollView contentContainerStyle={styles.list}>
                    {
                        pastGuesses.map((guess, index)=>listItem(guess, pastGuesses.length - index, index))
                    }
                </ScrollView> */}
                <FlatList contentContainerStyle={styles.list} keyExtractor={(item, index) => index.toString()} data={pastGuesses} renderItem={listItem.bind(this, pastGuesses.length)}></FlatList>
            </View>
        </View>
    )
};
const listItem = (listLength, itemData)=>{
    return (
      <View style={styles.listItem}>
        <BodyText>#{listLength - itemData.index}</BodyText>
        <BodyText>{itemData.item}</BodyText>
      </View>
    );
}
const styles = StyleSheet.create({
    screen:{
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    buttonsContainer:{
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: Dimensions.get("window").height > 600 ? 20 : 10,
        width: 400,
        maxWidth: "80%"
    },
    listContainer: {
        flex: 1,
        width: Dimensions.get("window").width > 350 ? "60%" : "80%"
    },
    list: {
        flexGrow: 1,
        //alignItems: "center",
        justifyContent: "flex-end"
    },
    listItem:{
        backgroundColor: "white",
        padding: 15,
        marginVertical: 10,
        borderColor: "#ccc",
        borderWidth: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        width: "100%"
        
    }
});
export default GameScreen;