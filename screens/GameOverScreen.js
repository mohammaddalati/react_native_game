import React from "react";
import { View, StyleSheet, Image, Dimensions, ScrollView } from "react-native";
import { BodyText, TitleText } from "../components/Text";
import colors from "../constants/colors";
import CustomButton from "../components/Button";

const GameOverScreen = (props) => {
  return (
    <ScrollView>
      <View style={styles.screen}>
        <TitleText style={styles.title}>The Game is Over</TitleText>
        <Image
          style={styles.imageStyle}
          source={require("../assets/success.png")}
          resizeMode="cover"
        />
        <BodyText style={styles.result}>
          Your Needed Number{" "}
          <TitleText style={styles.chosenNumber}>{props.userChoice}</TitleText>{" "}
          with number of attempts{" "}
          <TitleText style={styles.numberOfAttempts}>
            {props.attempts}
          </TitleText>
        </BodyText>
        <BodyText></BodyText>
        <CustomButton onPress={props.resetGame}>New Game</CustomButton>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageStyle: {
    width: Dimensions.get("window").width * 0.7,
    height: Dimensions.get("window").width * 0.7,
    borderRadius: 150,
    borderColor: "black",
    marginVertical: Dimensions.get("window").height / 30,
    borderWidth: 2,
  },
  title: {
    fontSize: 20,
  },
  result: {
    marginVertical: 10,
    marginHorizontal: Dimensions.get("window").height / 60,
    textAlign: "center",
    fontSize: Dimensions.get("window").height < 400 ? 16 : 20,
  },
  chosenNumber: {
    color: colors.primary,
  },
  numberOfAttempts: {
    color: colors.accent,
  },
});
export default GameOverScreen;
