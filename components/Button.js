import React from "react"
import {View, TouchableOpacity, StyleSheet} from "react-native"
import { BodyText } from "./Text"
import colors from "../constants/colors"

const CustomButton = (props)=>{
    return <TouchableOpacity onPress={props.onPress} activeOpacity={0.6}>
        <View style={styles.button}>
            <BodyText style={styles.buttonText}>{props.children}</BodyText>
        </View>
    </TouchableOpacity>
}
export default CustomButton;

const styles = StyleSheet.create({
    button:{
        backgroundColor: colors.primary,
        paddingVertical: 12,
        paddingHorizontal: 30,
        borderRadius: 50
    },
    buttonText:{
        color: "white",
        fontSize: 18,
        textAlign: "center"
    }
})