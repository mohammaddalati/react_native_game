import React from "react";
import {View, StyleSheet} from "react-native";

const Card = props=>{
    return(
    <View style={{...styles.container, ...props.style}}>
        {props.children}
    </View>
 )
}

const styles = StyleSheet.create({
    container:{
        padding: 20,
        elevation: 5,
        shadowColor: "black",
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: "white",
        borderRadius: 16
        
    }
});

export default Card;