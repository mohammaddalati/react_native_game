import React from "react"
import {Text, StyleSheet} from "react-native"

export const BodyText = (props)=>{
    return <Text style={{...styles.textStyle, ...props.style}}>{props.children}</Text>
}
export const TitleText = (props)=>{
    return <Text style={{...styles.titleText, ...props.style}}>{props.children}</Text>
}
const styles = StyleSheet.create({
    textStyle: {
        fontFamily: "open-sans"
    },
    titleText: {
        fontFamily: "open-sans-bold"
    }
})