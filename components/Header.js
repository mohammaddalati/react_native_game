import React from "react";
import {Text, View, StyleSheet} from "react-native";
import Colors from "../constants/colors.js"
import { TitleText } from "./Text.js";
const Header = (props)=>{
    return(
        <View style={styles.headerContainer}>
            <TitleText style={styles.headerContents}>{props.title}</TitleText>
        </View>
    );
}
const styles = StyleSheet.create({

    headerContainer: {
        height: 90,
        width: "100%",
        paddingTop: 36,
        backgroundColor: Colors.primary,
        alignItems: "center",
        justifyContent: "center"
    },
    headerContents: {
        color: "black",
        fontSize: 18
    }
})

export default Header;